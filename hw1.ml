(*1.1*)
let rec pow x n = match n with 
  | 0 -> 1
  | 1 -> x
  | _ -> x * pow x (n-1);;

let rec float_pow x n = match n with
  | 0 -> 1.0
  | 1 -> x
  | _ -> x *. float_pow  x (n-1);;

(*1.2*)
let rec compress l = match l with
  | a::(b::_ as temp) -> if a = b then compress temp else a::compress temp
  | _ -> l;;

(*1.3*)
let rec remove_if mylist predicate = match mylist with 
  | [] -> []
  | a :: b -> if predicate a then remove_if b predicate else a::remove_if b predicate;;


(*1.4*)
let rec slice lst i j = match lst with
    | [] -> []
    | h :: t -> if (j > List.length lst) then slice lst i (List.length lst) 
                else if i >= j then []            
                else if i = 0 then h :: (slice t 0 (j - 1)) 
                else slice t (i - 1) (j - 1);;


(*1.5*)
let rec equiv_on f g lst=
  if lst = []
      then true
  else
      if (f List.hd lst) = (g List.hd lst)
          then (equiv_on f g (List.tl lst))
      else
          false;;


(*1.6*)
let goldbach n = 
  let rec aux d =
      if is_prime d && is_prime (n-d) then (d, n-d)
      else aux (d+1) in
  aux 2;;



(*1.7*)
let equiv_on f g lst = if List.map lst = List.map g lst then true else false;;


(*1.8*)
let rec pairwisefilter cmp lst=
  match lst with
  |[] -> lst  
  | a::b::t -> (cmp a b):: (pairwisefilter cmp t)
  | _ -> lst


(*1.9*)
let rec polynomial lst z = match lst with 
  | [] -> 0
  | a::b::c->((fun(x,y)-> x* (pow z y)) a)+ (polynomial b z));;



(*1.10*)
let rec powerset = function
  | [] -> [[]]
  |a::b -> powerset b @List.map (fun x -> a::x)(powerset b);;