type bool_expr =
	| Lit of string
	| Not of bool_expr
	| And of bool_expr * bool_expr
  | Or of bool_expr * bool_expr;;

let rec evluate a b av bv exp = match exp with
  |Lit s -> if literal = a then av else bv 
  |Not exp -> not (evalute a b av bv exp)
  |And (exp1, exp2) -> (evalute a b av bv exp1)&& (evalute a b av bv exp2)
  |Or(exp1, exp2) -> (evalute a b av bv exp1)|| (evalute a b av bv exp2);;

let truth_table a b expression = 
  [
    (true,  true,  solve a b true  true  expression); 
     (true,  false, solve a b true false expression);
     (false, true,  solve a b false true  expression);
     (false, false, solve a b false false expression)]
  ];;